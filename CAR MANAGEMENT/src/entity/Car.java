/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

/**
 *
 * @author computer
 */
public class Car {
    
    private int idcar;
    private String carmodel;
    private double price;
    private double resalevalue;
    
    public Car(int idcar,String carmodel,double price,double resalevalue)
    {
        this.idcar=idcar;
        this.carmodel=carmodel;
        this.price=price;
        this.resalevalue=resalevalue;
    }

   
    
    

   /* public int getId() {
        return idcar;
    }

    public void setId(int id) {
        this.idcar = idcar;
    }

    public String getModel() {
        return carmodel;
    }

    public void setModel(String model) {
        this.carmodel = carmodel;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Float getResalevalue() {
        return resalevalue;
    }

    public void setResalevalue(Float resalevalue) {
        this.resalevalue = resalevalue;
    }
    */

    public int getIdcar() {
        return idcar;
    }

    public void setIdcar(int idcar) {
        this.idcar = idcar;
    }

    public String getCarmodel() {
        return carmodel;
    }

    public void setCarmodel(String carmodel) {
        this.carmodel = carmodel;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getResalevalue() {
        return resalevalue;
    }

    public void setResalevalue(double resalevalue) {
        this.resalevalue = resalevalue;
    }
    
    
}
